extends Node
class_name BulletPattern

@export var canShoot : bool

func Init (spawnNode):
	pass

func Shoot (bulletScenePath : String, spawnNode : Node2D):
	if !canShoot:
		return []
	var output : Array[Bullet]
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	bullet.global_transform = spawnNode.global_transform
	Room.instance.add_child(bullet)
	return output

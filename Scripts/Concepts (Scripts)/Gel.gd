extends Node2D
class_name Gel

@export var sourceNode : Node2D
@export var makeRadius : float
@export var makeDistance : float
@export var duration : float
@export var collisionPolygon : CollisionPolygon2D
@export var line : Line2D
var previousSourcePosition : Vector2
var makePointTimes : PackedFloat32Array
var time : float

func _ready ():
	Reparent.call_deferred()
	previousSourcePosition = sourceNode.global_position
	line.add_point(previousSourcePosition)
	makePointTimes.append(0)

func Reparent ():
	reparent(Room.instance)
	global_position = Vector2.ZERO
	global_rotation_degrees = 0
	global_scale = Vector2.ONE

func _physics_process (deltaTime : float):
	if sourceNode != null && (sourceNode.global_position - previousSourcePosition).length_squared() > makeDistance * makeDistance:
		previousSourcePosition = sourceNode.global_position
		line.add_point(previousSourcePosition)
		makePointTimes.append(time)
	time += deltaTime
	if makePointTimes.size() > 0:
		while time >= makePointTimes[0] + duration:
			makePointTimes.remove_at(0)
			if makePointTimes.size() == 0:
				free()
				return
			line.remove_point(0)

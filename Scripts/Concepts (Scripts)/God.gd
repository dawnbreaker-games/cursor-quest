extends Node
class_name God

@export var points : int
@export var effects : Array[GodEffect]
@export var color : Color

func _ready ():
	effects.append_array(NodeExtensions.GetNodes(GodEffect, false, self))

func AddPoints (amount : int):
	points += amount
	for effect in effects:
		if effect.CanBeApplied(points):
			if effect.scalesWithPoints || (!%Player.godEffectsIHaveEnoughPointsFor.has(effect) && !%Player.HasGodEffectOfSameTier(effect)):
				if effect.isBad:
					GodEffectMenu.godsForTiersThatWentDown.append(self)
				else:
					GodEffectMenu.godsForTiersThatWentUp.append(self)
			if !%Player.godEffectsIHaveEnoughPointsFor.has(effect):
				%Player.godEffectsIHaveEnoughPointsFor.append(effect)
		else:
			if effect.scalesWithPoints || %Player.godEffectsIHaveEnoughPointsFor.has(effect):
				GodEffectMenu.godsForTiersThatWentDown.append(self)
			%Player.godEffectsIHaveEnoughPointsFor.erase(effect)
	if GodEffectMenu.godsForTiersThatWentUp.size() > 0 || GodEffectMenu.godsForTiersThatWentDown.size() > 0:
		%GodEffectMenu.Open ()

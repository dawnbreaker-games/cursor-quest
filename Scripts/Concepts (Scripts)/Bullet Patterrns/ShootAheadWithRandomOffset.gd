extends BulletPattern
class_name ShootAheadWithRandomOffset

@export var randomOffsetRange : FloatRange

func Shoot (bulletScenePath : String, spawnNode : Node2D):
	var output : Array[Bullet]
	var bullet = load(bulletScenePath).instantiate()
	output.append(bullet)
	bullet.global_transform = spawnNode.global_transform
	bullet.global_rotation_degrees += randf_range(randomOffsetRange.min, randomOffsetRange.max)
	get_tree().current_scene.add_child(bullet)
	return output

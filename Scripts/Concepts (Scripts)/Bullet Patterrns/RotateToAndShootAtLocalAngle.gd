extends BulletPattern
class_name RotateToAndShootAtLocalAngle

@export var degrees : float

func Shoot (bulletScenePath : String, spawnNode : Node2D):
	spawnNode.rotation_degrees = degrees
	return super.Shoot(bulletScenePath, spawnNode)

extends BulletPattern
class_name ShootAheadWithOffset

@export var offset : float

func Shoot (bulletScenePath : String, spawnNode : Node2D):
	spawnNode.global_rotation_degrees += offset
	var output = super.Shoot(bulletScenePath, spawnNode)
	spawnNode.global_rotation_degrees -= offset
	return output

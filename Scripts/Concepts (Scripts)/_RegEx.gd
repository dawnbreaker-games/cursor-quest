extends Node
class_name _RegEx

@export_multiline var text : String

func Search (str : String, offset : int = 0, end : int = 0):
	var regEx = RegEx.create_from_string(text)
	return regEx.search(regEx, offset, end)

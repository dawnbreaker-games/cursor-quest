@tool
extends VisualShaderNodeCustom
class_name PointOnCircle

func _get_name ():
	return "PointOnCircle"
	
func _get_input_port_name (port : int):
	match port:
		0:
			return "center"
		1:
			return "degrees"
		2:
			return "radius"

func _get_global_code (mode : Shader.Mode):
	return """
		vec2 FromFacingAngle (float facingAngle) {
			return normalize(vec2(cos(radians(facingAngle)), sin(radians(facingAngle))));
		}
		
		vec2 PointOnCircle (vec2 center, float degrees, float radius) {
			return center + FromFacingAngle(degrees) * radius;
		}
	"""

func _get_code (inputs : Array[String], outputs : Array[String], mode : Shader.Mode, type : VisualShader.Type):
	return outputs[0] + " = PointOnCircle (%s, %s, %s);" % [ inputs[0], inputs[1], inputs[2] ]

func _get_input_port_count ():
	return 3

func _get_input_port_default_value (port : int):
	match port:
		0:
			return Vector2.ZERO
		1:
			return 0
		2:
			return 1

func _get_input_port_type (port : int):
	match port:
		0:
			return PORT_TYPE_VECTOR_2D
		1:
			return PORT_TYPE_SCALAR
		2:
			return PORT_TYPE_SCALAR

func _get_output_port_type (port : int):
	return PORT_TYPE_VECTOR_2D

func _get_output_port_count ():
	return 1

extends Menu
class_name MapMenu

@export var roomIndicatorsParent : Control
@export var roomIndicatorsDict : Dictionary
@export var roomIndicatorsSeparation : float
@export var playerIndicator : Control
static var instance : MapMenu

func _ready ():
	super._ready ()
	instance = self

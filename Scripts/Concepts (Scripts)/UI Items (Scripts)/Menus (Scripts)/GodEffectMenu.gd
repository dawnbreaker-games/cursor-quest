extends Menu
class_name GodEffectMenu

@export var chooseRewardLabel : Label
@export var lostOrGotBadGodEffectLabel : Label
@export var effectChoicesParent : Node
@export var getRewardUIParent : Node
@export var lostOrGotBadGodEffectUIParent : Node
static var godsForTiersThatWentUp : Array[God]
static var godsForTiersThatWentDown : Array[God]
const GOD_EFFECT_CHOICE_BUTTON_SCENE = preload("res://Scenes/Concepts (Scenes)/UI Items (Scenes)/God Effect Choice Button.tscn")

func Open ():
	super.Open ()
	if godsForTiersThatWentUp.size() > 0:
		var god = godsForTiersThatWentUp[0]
		godsForTiersThatWentUp.remove_at(0)
		var effectToLose
		for effect in god.effects:
			if !effect.CanBeApplied(god.points) && %Player.godEffectsDict[god].has(effect):
				effectToLose = effect
				break
		if effectToLose != null:
			LoseGodEffect (god, effectToLose)
		else:
			chooseRewardLabel.text = "Choose A Reward From " + god.name
			for i in range(effectChoicesParent.get_child_count()):
				var effectChoiceButton = effectChoicesParent.get_child(i)
				effectChoiceButton.queue_free()
			var shouldClose = true
			for effect in god.effects:
				if effect.CanBeApplied(god.points) && !%Player.HasGodEffectOfSameTier(effect):
					var godEffectChoiceButton = GOD_EFFECT_CHOICE_BUTTON_SCENE.instantiate()
					godEffectChoiceButton.get_child(0).text = effect.GetDescription()
					var callable = Callable(self, "OnChoseGodEffect")
					callable = callable.bind(god, effect)
					godEffectChoiceButton.pressed.connect(callable)
					effectChoicesParent.add_child(godEffectChoiceButton)
					shouldClose = false
			getRewardUIParent.visible = true
			lostOrGotBadGodEffectUIParent.visible = false
			if shouldClose:
				Close ()
	elif godsForTiersThatWentDown.size() > 0:
		var god = godsForTiersThatWentDown[0]
		godsForTiersThatWentDown.remove_at(0)
		var effectToLose
		for effect in god.effects:
			if !effect.CanBeApplied(god.points) && %Player.godEffectsDict[god].has(effect):
				effectToLose = effect
				break
		if effectToLose != null:
			LoseGodEffect (god, effectToLose)
		else:
			var effectsToGain : Array[GodEffect]
			for effect in god.effects:
				if effect.CanBeApplied(god.points) && !%Player.HasGodEffectOfSameTier(effect):
					effectsToGain.append(effect)
			if effectsToGain.size() == 0:
				print("Huh")
				Open ()
				return
			var effect = effectsToGain[randi_range(0, effectsToGain.size() - 1)]
			effect.Apply (god.points)
			lostOrGotBadGodEffectLabel.text = "You gained: " + effect.GetDescription()
			AddNotificationSuffix (god, effect)
			getRewardUIParent.visible = false
			lostOrGotBadGodEffectUIParent.visible = true
	else:
		Close ()

func LoseGodEffect (god : God, effect : GodEffect):
	effect.Unapply (god.points)
	lostOrGotBadGodEffectLabel.text = "You lost: " + effect.GetDescription()
	AddNotificationSuffix (god, effect)
	getRewardUIParent.visible = false
	lostOrGotBadGodEffectUIParent.visible = true

func AddNotificationSuffix (god : God, effect : GodEffect):
	lostOrGotBadGodEffectLabel.text += " because you have " 
	if effect.pointsRequired == god.points:
		lostOrGotBadGodEffectLabel.text += str(god.points) + " favor points with " + god.name
	else:
		if effect.isBad:
			lostOrGotBadGodEffectLabel.text += "more "
		else:
			lostOrGotBadGodEffectLabel.text += "less "
		lostOrGotBadGodEffectLabel.text += "than " + str(effect.pointsRequired) + " favor points with " + god.name

func OnChoseGodEffect (god : God, effect : GodEffect):
	effect.Apply (god.points)
	Open ()

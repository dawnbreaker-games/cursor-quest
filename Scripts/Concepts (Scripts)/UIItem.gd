extends Control
class_name UIItem

@export var pivotNormalized : Vector2

func _ready ():
	if get_parent_control() == null:
		_Camera2D.instance = get_tree().current_scene.find_child("_Camera2D")
		scale /= _Camera2D.instance.zoom
		scale = Vector2.ONE * min(scale.x, scale.y)
		global_position += _Camera2D.instance.global_position
	pivot_offset = pivotNormalized * size

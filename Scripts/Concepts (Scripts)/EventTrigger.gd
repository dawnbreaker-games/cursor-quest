extends Node2D
class_name EventTrigger

@export var canTriggerWhenPaused : bool
@export var triggerOnAnythingPressed : bool
@export var triggerOnInputAction : String
@export var triggerOnInputActionState : InputActionState
@export var button : Button
var wasAnythingPressed : bool

func _input (inputEvent : InputEvent):
	if button.disabled || (!canTriggerWhenPaused && GameManager.paused):
		return
	if triggerOnAnythingPressed:
		var anythingPressed = Input.is_anything_pressed()
		if triggerOnInputActionState == InputActionState.JustPressed:
			if anythingPressed && !wasAnythingPressed:
				Trigger ()
		elif triggerOnInputActionState == InputActionState.JustReleased:
			if !anythingPressed && wasAnythingPressed:
				Trigger ()
		elif triggerOnInputActionState == InputActionState.Pressed:
			if anythingPressed:
				Trigger ()
		elif !anythingPressed:
			Trigger ()
		wasAnythingPressed = anythingPressed
	elif triggerOnInputAction != "":
		if triggerOnInputActionState == InputActionState.JustPressed:
			if Input.is_action_just_pressed(triggerOnInputAction):
				Trigger ()
		elif triggerOnInputActionState == InputActionState.JustReleased:
			if Input.is_action_just_released(triggerOnInputAction):
				Trigger ()
		elif triggerOnInputActionState == InputActionState.Pressed:
			if Input.is_action_pressed(triggerOnInputAction):
				Trigger ()
		elif !Input.is_action_pressed(triggerOnInputAction):
			Trigger ()

func Trigger ():
	button.pressed.emit()

enum InputActionState {
	JustPressed,
	JustReleased,
	Pressed,
	NotPressed
}

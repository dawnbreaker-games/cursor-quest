extends GodEffect
class_name AddToHagglingGodEffect

@export var amount : float
var addToHaggling : float

func Apply (points : int):
	super.Apply (points)
	if scalesWithPoints:
		Player.instance.SetHaggling (Player.instance.haggling - addToHaggling)
		if subtractPointsRequired:
			addToHaggling = amount * (points - pointsRequired)
		else:
			addToHaggling = amount * points
		Player.instance.SetHaggling (Player.instance.haggling + addToHaggling)
	else:
		Player.instance.SetHaggling (Player.instance.haggling + amount)

func Unapply (points : int):
	super.Unapply (points)
	if scalesWithPoints:
		Player.instance.SetHaggling (Player.instance.haggling + addToHaggling)
		if subtractPointsRequired:
			addToHaggling = amount * (points - pointsRequired)
		else:
			addToHaggling = amount * points
		Player.instance.SetHaggling (Player.instance.haggling - addToHaggling)
	else:
		Player.instance.SetHaggling (Player.instance.haggling - amount)

func GetDescription ():
	var output = ""
	if scalesWithPoints:
		if !subtractPointsRequired:
			output = "Gain " + str(amount * pointsRequired) + " haggling\n"
		output += "Each time you get a favor point from this god gain " + str(amount) + " haggling\n"
	else:
		output = "Gain " + str(amount) + " haggling\n"
	output += "The costs of items in the shop are always divided by (haggling + 1)"
	return output

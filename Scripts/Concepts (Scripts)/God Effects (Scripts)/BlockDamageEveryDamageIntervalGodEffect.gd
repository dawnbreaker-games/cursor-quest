extends GodEffect
class_name BlockDamageEveryDamageIntervalGodEffect

@export var interval : float
var damageTillBlock : int

func _ready ():
	damageTillBlock = interval

func GetDescription ():
	return "You avoid taking any damage every " + str(interval) + " times you would take damage"

extends GodEffect
class_name AddToDamageMultiplierEachRoomGodEffect

@export var amount : float

func GetDescription ():
	return "Adds " + str(amount) + " to a multiplier for your regular attack damage and radiation damage after every room you clear of enemies. The multiplier is currently 1."

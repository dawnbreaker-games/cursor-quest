extends Node2D
class_name Game

@export var currentDifficulty : float
@export var addToRoomDifficulty : float
@export var difficultyPerHeartRange : IntRange
@export var addToMultiplyDifficultyTillHeart : float
@export var roomsPerShopRoomRange : IntRange
@export var roomsPerShrineRoomRange : IntRange
@export var totalShopCost : int
@export var addToTotalShopCost : int
@export var multiplyShopCosts : float
@export var addToMultiplyShopCosts : float
@export var cashPerEnemyDifficulty : float
@export var gods : Array[God]
@export var hideOnExitRoom : CanvasItem
static var instance : Game
var roomsDict : Dictionary
var difficultyTillHeart : float
var multiplyDifficultyTillHeart = 1.0
var roomsTillShopRoom : int
var roomsTillShrineRoom : int
const ROOM_SCENE = preload("res://Scenes/Objects (Scenes)/Room.tscn")
const SHOP_ROOM_SCENE = preload("res://Scenes/Objects (Scenes)/Rooms (Scenes)/Shop Room.tscn")
const SHRINE_ROOM_SCENE = preload("res://Scenes/Objects (Scenes)/Rooms (Scenes)/Shrine Room.tscn")
const ROOM_INDICATOR_SCENE = preload("res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Room Indicator.tscn")
const SHOP_ROOM_INDICATOR_SCENE = preload("res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Room Indicators (Scenes)/Shop Room Indicator.tscn")
const SHRINE_ROOM_INDICATOR_SCENE = preload("res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Room Indicators (Scenes)/Shrine Room Indicator.tscn")
const ENEMY_SCENES = [ preload("res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 1.tscn"), preload("res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 2.tscn"), preload("res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 3.tscn"), preload("res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 4.tscn"), preload("res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 5.tscn"), preload("res://Scenes/Objects (Scenes)/Enemies (Scenes)/Enemy 6.tscn") ]
const HEART_SCENE = preload("res://Scenes/Objects (Scenes)/Items (Scenes)/Heart.tscn")
const ITEM_SCENES = [ preload("res://Scenes/Objects (Scenes)/Items (Scenes)/Heart.tscn"), preload("res://Scenes/Objects (Scenes)/Items (Scenes)/Steroids.tscn"), preload("res://Scenes/Objects (Scenes)/Items (Scenes)/Shield Item.tscn"), preload("res://Scenes/Objects (Scenes)/Items (Scenes)/Useable Items (Scenes)/Energy Sphere.tscn"), preload("res://Scenes/Objects (Scenes)/Items (Scenes)/Useable Items (Scenes)/Alien Friend.tscn") ]

func _ready ():
	instance = self
	Enemy.instances.clear()
	for god in gods:
		%Player.godEffectsDict[god] = []
	Input.mouse_mode = Input.MOUSE_MODE_CONFINED_HIDDEN
	difficultyTillHeart = randf_range(difficultyPerHeartRange.min, difficultyPerHeartRange.max) * multiplyDifficultyTillHeart
	roomsTillShopRoom = randi_range(roomsPerShopRoomRange.min, roomsPerShopRoomRange.max)
	roomsTillShrineRoom = randi_range(roomsPerShrineRoomRange.min, roomsPerShrineRoomRange.max)
	var room = NewRoom(Vector2i.ZERO)
	room.cleared = true

func ExitRoom (exitDirection : Vector2i):
	Room.instance.visible = false
	for bullet in Bullet.instances:
		if bullet != null:
			bullet.queue_free()
	Bullet.instances.clear()
	var location = Room.instance.location + exitDirection
	if !roomsDict.has(location):
		NewRoom (location)
	else:
		Room.instance = roomsDict[location]
		Room.instance.visible = true
	Heart.heartCountInCurrentRoom = NodeExtensions.GetNodes(Heart, false, Room.instance).size()
	%MapMenu.playerIndicator.global_position = %MapMenu.roomIndicatorsDict[location].global_position
	%MapMenu.roomIndicatorsParent.global_position = -location * %MapMenu.roomIndicatorsSeparation / 2
	hideOnExitRoom.visible = false

func NewRoom (location : Vector2i):
	var chooseFromRooms : Array[Callable]
	if roomsTillShopRoom == 0:
		chooseFromRooms.append(NewShopRoom)
	if roomsTillShrineRoom == 0:
		chooseFromRooms.append(NewShrineRoom)
	if chooseFromRooms.size() > 0:
		return chooseFromRooms[randi_range(0, chooseFromRooms.size() - 1)].call(location)
	var room = ROOM_SCENE.instantiate()
	room.location = location
	room.difficulty = currentDifficulty
	add_child(room)
	roomsDict[location] = room
	var difficultyRemaining = currentDifficulty
	var enemyScenesRemaining = ENEMY_SCENES.duplicate()
	var spawnRect = room.GetRect()
	spawnRect = spawnRect.grow(-room.spawnBorder)
	while enemyScenesRemaining.size() > 0:
		var enemySceneIndex = randi_range(0, enemyScenesRemaining.size() - 1)
		var enemyScene = enemyScenesRemaining[enemySceneIndex]
		var enemy = enemyScene.instantiate()
		if enemy.difficulty <= difficultyRemaining:
			enemy.global_position = RectExtensions.GetRandomPoint(spawnRect)
			add_child(enemy)
			difficultyRemaining -= enemy.difficulty
		else:
			enemy.free()
			enemyScenesRemaining.remove_at(enemySceneIndex)
	Room.instance = room
	currentDifficulty += addToRoomDifficulty
	var roomIndicator = ROOM_INDICATOR_SCENE.instantiate()
	roomIndicator.position = location * %MapMenu.roomIndicatorsSeparation
	roomIndicator.room = room
	%MapMenu.roomIndicatorsDict[location] = roomIndicator
	%MapMenu.roomIndicatorsParent.add_child(roomIndicator)
	return room

func NewShopRoom (location : Vector2i):
	var shopRoom = SHOP_ROOM_SCENE.instantiate()
	shopRoom.location = location
	add_child(shopRoom)
	roomsDict[location] = shopRoom
	Room.instance = shopRoom
	roomsTillShopRoom = randi_range(roomsPerShopRoomRange.min, roomsPerShopRoomRange.max)
	var roomIndicator = SHOP_ROOM_INDICATOR_SCENE.instantiate()
	roomIndicator.position = location * %MapMenu.roomIndicatorsSeparation
	roomIndicator.room = shopRoom
	%MapMenu.roomIndicatorsDict[location] = roomIndicator
	%MapMenu.roomIndicatorsParent.add_child(roomIndicator)
	return shopRoom

func NewShrineRoom (location : Vector2i):
	var shrineRoom = SHRINE_ROOM_SCENE.instantiate()
	shrineRoom.location = location
	add_child(shrineRoom)
	roomsDict[location] = shrineRoom
	Room.instance = shrineRoom
	roomsTillShrineRoom = randi_range(roomsPerShrineRoomRange.min, roomsPerShrineRoomRange.max)
	var god = gods[randi_range(0, gods.size() - 1)]
	shrineRoom.shrine.god = god
	shrineRoom.shrine.canvasItem.modulate = god.color
	var roomIndicator = SHRINE_ROOM_INDICATOR_SCENE.instantiate()
	roomIndicator.shrineIndicator.modulate = god.color
	roomIndicator.position = location * %MapMenu.roomIndicatorsSeparation
	roomIndicator.room = shrineRoom
	%MapMenu.roomIndicatorsDict[location] = roomIndicator
	%MapMenu.roomIndicatorsParent.add_child(roomIndicator)
	return shrineRoom

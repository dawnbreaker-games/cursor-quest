extends Node2D
class_name Bullet

@export var moveSpeed : float
@export var damage : int
@export var damageSpeed : int
@export var range : float
@export var duration : float
@export var despawnMode : DespawnMode
@export var rigidBody : RigidBody2D
@export var area : Area2D
static var instances : Array[Bullet]
var damaging : Array[Destructable]
var callOnUnpausedFrame = Callable()
var callOnDestroy = Callable()

func _ready ():
	instances.append(self)
	tree_exiting.connect(OnDestroy)
	if despawnMode == DespawnMode.None || rigidBody == null:
		set_physics_process(false)
	if rigidBody != null:
		rigidBody.body_entered.connect(OnEnterBody)
	if area != null:
		area.body_entered.connect(OnEnterArea)
		area.body_exited.connect(OnExitArea)

func _physics_process (deltaTime : float):
	if GameManager.paused:
		return
	callOnUnpausedFrame.call_deferred(self)
	rigidBody.linear_velocity = VectorExtensions.FromFacingAngle(rigidBody.global_rotation_degrees) * moveSpeed
	for i in range(damaging.size()):
		var _damaging = damaging[i]
		if _damaging == null:
			damaging.remove_at(i)
			i -= 1
		else:
			_damaging.TakeDamage (damageSpeed * deltaTime)
	if despawnMode == DespawnMode.Range:
		range -= moveSpeed * deltaTime
		if range <= 0:
			OnDestroy ()
			queue_free()
	else:
		duration -= deltaTime
		if duration <= 0:
			OnDestroy ()
			queue_free()
		elif rigidBody.global_position.x < -Room.instance.size.x / 2 || rigidBody.global_position.x > Room.instance.size.x / 2 || rigidBody.global_position.y < -Room.instance.size.y / 2 || rigidBody.global_position.y > Room.instance.size.y / 2:
			OnDestroy ()
			queue_free()

func OnDestroy ():
	instances.erase(self)
	if callOnDestroy != Callable() && self != null:
		callOnDestroy.call_deferred(self)

func OnEnterArea (node : Node2D):
	damaging.append(node.get_parent())

func OnExitArea (node : Node2D):
	damaging.erase(node.get_parent())

func OnEnterBody (node : Node2D):
	OnDestroy ()
	queue_free()

enum DespawnMode {
	None,
	Range,
	Time
}

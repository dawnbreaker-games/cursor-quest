extends Destructable
class_name Enemy

@export var moveSpeed : float
@export var reloadSpeed : float
@export var followDistance : float
@export var bulletPatternEntry : BulletPatternEntry
@export var rigidBody : RigidBody2D
@export var shootTransformNode : Node
@export var difficulty : float
@export var targetedIndicator : CanvasItem
static var instances : Array[Enemy]
var deltaTime : float
var shootTimer : float
const BLOOD_SCENE = preload("res://Scenes/Objects (Scenes)/Blood.tscn")

func _ready ():
	super._ready ()
	shootTimer = reloadSpeed * randf()
	instances.append(self)

func _exit_tree ():
	instances.erase(self)

func _physics_process (deltaTime : float):
	if GameManager.paused:
		return
	self.deltaTime = deltaTime
	HandleMove ()
	HandleAttack ()

func HandleMove ():
	var toPlayer = Player.instance.global_position - rigidBody.global_position
	if toPlayer.length_squared() > followDistance * followDistance:
		rigidBody.linear_velocity = toPlayer.normalized() * moveSpeed
	else:
		rigidBody.linear_velocity = -toPlayer.normalized() * moveSpeed
	rigidBody.look_at(rigidBody.global_position + toPlayer)

func HandleAttack ():
	if reloadSpeed == -1:
		return
	shootTimer -= deltaTime
	if shootTimer <= 0:
		shootTimer += reloadSpeed
		bulletPatternEntry.Shoot ()

func Die ():
	super.Die()
	Game.instance.difficultyTillHeart -= difficulty
	if Game.instance.difficultyTillHeart <= 0:
		var heart = Game.HEART_SCENE.instantiate()
		heart.global_position = rigidBody.global_position
		Room.instance.add_child(heart)
		Game.instance.difficultyTillHeart = randf_range(Game.instance.difficultyPerHeartRange.min, Game.instance.difficultyPerHeartRange.max) * Game.instance.multiplyDifficultyTillHeart
	Player.instance.score += difficulty
	if Player.instance.score > Player.instance.highscore:
		Player.instance.highscore = Player.instance.score
	Player.instance.AddCash (difficulty * Game.instance.cashPerEnemyDifficulty * Player.instance.enemyCashMultiplier)
	instances.erase(self)
	if instances.size() == 0:
		Room.instance.Clear ()

extends Destructable
class_name Player

@export var rayCast : RayCast2D
@export var attackSpeed : float
@export var damage : float
@export var maxAttackBounces : float
@export var attackRange : float
@export var hpImagesParent : Node
@export var maxHpImagesParent : Node
@export var itemsParent : Node
@export var freeRerolls : float
@export var haggling : float
@export var enemyCashMultiplier : float
@export var cash : float
@export var cashLabel : Label
@export var prayingTo : Array[God]
@export var distributePointsToGodsImPrayingTo : int
@export var deathMenu : Menu
@export var deathLabel : Label
@export var itemCooldownBar : TextureProgressBar
@export var itemCooldownLabel : Label
@export var radiationDamage : float
@export var radiationDistanceScale : float
@export var invulnerableIndicator : CanvasItem
@export var extraDamageIndicator : CanvasItem
@export var timeMultiplierWhileSpedUp : float
static var instance : Player
static var highscore : float
var multiplyDamage = 1.0
var maxCash = INF
var previousEnemyCashMultiplier : float
var deltaTime : float
var moving = true
var attacking : Enemy
var attackTimer : float
var previousPosition : Vector2
var previousHitEnemyCollider : Object
var previousHitItem : Item
var items : Array[Item]
var godEffectsDict : Dictionary
var godEffectsIHaveEnoughPointsFor : Array[GodEffect]
var score : float
var attackBouncesRemaining : int
var attackRangeRemaining : float
var attackingEnemies : Array[Enemy]
var invulnerable : bool
const SWORD_BEAM_SCENE = preload("res://Scenes/Objects (Scenes)/God Sword Beam.tscn")
const HP_IMAGE_SCENE = preload("res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Hp Image.tscn")
const MAX_HP_IMAGE_SCENE = preload("res://Scenes/Concepts (Scenes)/UI Items (Scenes)/Max Hp Image.tscn")
const REPLACE_WITH_SCORE_INDICATOR = "_"
const REPLACE_WITH_HIGHSCORE_INDICATOR = "|"

func _ready ():
	super._ready ()
	instance = self
	SetHaggling (haggling)
	SetEnemyCashMultiplier (enemyCashMultiplier)
	for i in range(0, hp):
		var hpImage = HP_IMAGE_SCENE.instantiate()
		hpImagesParent.add_child(hpImage)
		var maxHpImage = MAX_HP_IMAGE_SCENE.instantiate()
		maxHpImagesParent.add_child(maxHpImage)

func _physics_process (deltaTime : float):
	if get_tree() == null || GameManager.paused:
		return
	self.deltaTime = deltaTime
	var roomRect = Room.instance.GetRect()
	if attacking == null:
		attacking = NodeExtensions.GetClosestNode2DToPoint(get_global_mouse_position(), Enemy)
		if attacking != null:
			attacking.targetedIndicator.visible = true
	if moving:
		global_position = get_global_mouse_position().clamp(roomRect.position - Vector2i.ONE, roomRect.end + Vector2i.ONE)
	else:
		if attacking != null:
			attacking.targetedIndicator.visible = false
		var enemies = NodeExtensions.GetNodes(Enemy)
		if enemies.size() > 0:
			var closestEnemy = enemies[0]
			var closestDistanceSqr = (closestEnemy.rigidBody.global_position - get_global_mouse_position()).length_squared()
			for i in range(1, enemies.size()):
				var enemy = enemies[i]
				var distanceSqr = (enemy.rigidBody.global_position - get_global_mouse_position()).length_squared()
				if distanceSqr < closestDistanceSqr:
					closestDistanceSqr = distanceSqr
					closestEnemy = enemy
			attacking = closestEnemy
		if attacking != null:
			attacking.targetedIndicator.visible = true
	HandleAttack ()
	rayCast.global_position = previousPosition
	rayCast.target_position = rayCast.to_local(global_position)
	rayCast.force_raycast_update()
	var hitEnemy = false
	var hitItem = false
	var collider = rayCast.get_collider()
	while collider != null:
		if collider == previousHitEnemyCollider:
			hitEnemy = true
			rayCast.add_exception(collider)
		else:
			var colliderParent = collider.get_parent()
			if colliderParent == previousHitItem:
				hitItem = true
				rayCast.add_exception(collider)
			else:
				var bullet = colliderParent as Bullet
				if bullet != null:
					TakeDamage (bullet.damage)
					if bullet != null:
						bullet.free()
				else:
					var enemy = colliderParent as Enemy
					if enemy != null:
						hitEnemy = true
						rayCast.add_exception(collider)
						previousHitEnemyCollider = collider
						if enemy is ChargingEnemy:
							if enemy.charging:
								TakeDamage (enemy.damage)
					else:
						var collectable = colliderParent as Collectable
						if collectable != null:
							var item = collectable as Item
							if item == null:
								collectable.Collect ()
							else:
								previousHitItem = item
								hitItem = true
								item.descriptionLabel.visible = true
								rayCast.add_exception(collider)
		rayCast.global_position = rayCast.get_collision_point()
		rayCast.target_position = rayCast.to_local(global_position)
		rayCast.force_raycast_update()
		collider = rayCast.get_collider()
	if !hitEnemy:
		previousHitEnemyCollider = null
		if !hitItem:
			if previousHitItem != null:
				previousHitItem.descriptionLabel.visible = false
			previousHitItem = null
	rayCast.clear_exceptions()
	if !roomRect.has_point(global_position):
		if Room.instance.cleared:
			var exitDirection = Vector2i.ZERO
			if global_position.x < -Room.instance.size.x / 2:
				exitDirection.x = -1
				global_position.x += Room.instance.size.x
			elif global_position.x > Room.instance.size.x / 2:
				exitDirection.x = 1
				global_position.x -= Room.instance.size.x
			elif global_position.y < -Room.instance.size.y / 2:
				exitDirection.y = -1
				global_position.y += Room.instance.size.y
			elif global_position.y > Room.instance.size.y / 2:
				exitDirection.y = 1
				global_position.y -= Room.instance.size.y
			Input.warp_mouse(global_position * _Camera2D.instance.zoom + get_viewport_rect().size / 2)
			Game.instance.ExitRoom (exitDirection)
		else:
			Input.warp_mouse(global_position.clamp(roomRect.position, roomRect.end) * _Camera2D.instance.zoom + get_viewport_rect().size / 2)
	global_rotation_degrees = VectorExtensions.GetFacingAngle(to_global(Vector2.UP)) + 90
	previousPosition = global_position

func _input (inputEvent : InputEvent):
	if inputEvent is InputEventKey:
		if Input.is_action_just_pressed("Reset"):
			%GameManager.Save ()
			get_tree().reload_current_scene()
		elif Input.is_action_just_pressed("Next Level"):
			GameManager.NextLevel ()
		elif Input.is_action_just_pressed("Previous Level"):
			GameManager.PreviousLevel ()
		elif Input.is_action_just_pressed("Pause"):
			if PauseMenu.instance.open:
				PauseMenu.instance.Close ()
			else:
				PauseMenu.instance.Open ()
		elif Input.is_action_just_pressed("Map"):
			if %MapMenu.open:
				%MapMenu.Close ()
			else:
				%MapMenu.Open ()
		elif Input.is_action_just_pressed("Pray Menu"):
			if %PrayMenu.open:
				%PrayMenu.Close ()
			else:
				%PrayMenu.Open ()
		elif Input.is_action_just_pressed("Speed Up Time"):
			Engine.time_scale = timeMultiplierWhileSpedUp
			print(1)
		elif Input.is_action_just_released("Speed Up Time"):
			Engine.time_scale = 1
			print(2)
	elif inputEvent is InputEventMouseButton && !GameManager.paused:
		if Input.is_action_just_pressed("Stop Moving"):
			moving = false
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		elif Input.is_action_just_released("Stop Moving"):
			moving = true
			Input.mouse_mode = Input.MOUSE_MODE_CONFINED_HIDDEN
			Input.warp_mouse(global_position * _Camera2D.instance.zoom + get_viewport_rect().size / 2)

func HandleAttack ():
	if attacking == null && attackTimer <= 0:
		return
	if attackTimer > 0:
		attackTimer -= deltaTime
	if attacking != null && attackTimer <= 0:
		attackRangeRemaining = attackRange
		var toEnemy = attacking.rigidBody.global_position - global_position
		if toEnemy.length() <= attackRange:
			attackBouncesRemaining = maxAttackBounces
			attackingEnemies.clear()
			attackTimer += 1.0 / attackSpeed
			Attack (attacking, global_position)
	if radiationDamage > 0:
		HandleRadiation ()

func HandleRadiation ():
	for enemy in Enemy.instances:
		var enemyDistance = (enemy.rigidBody.global_position - global_position).length()
		enemy.TakeDamage (clampf((radiationDistanceScale - enemyDistance) / radiationDistanceScale, 0, 1) * radiationDamage * multiplyDamage * deltaTime)

func Attack (enemy : Enemy, fromPosition : Vector2):
	var swordBeam = SWORD_BEAM_SCENE.instantiate()
	var points : PackedVector2Array
	points.append(fromPosition)
	points.append(enemy.rigidBody.global_position)
	swordBeam.points = points
	Room.instance.add_child(swordBeam)
	enemy.TakeDamage (damage * multiplyDamage)
	var blood = Enemy.BLOOD_SCENE.instantiate()
	blood.global_position = enemy.rigidBody.global_position
	blood.look_at(blood.global_position + (blood.global_position - fromPosition))
	blood.emitting = true
	Room.instance.add_child(blood)
	attackRangeRemaining -= (enemy.rigidBody.global_position - global_position).length()
	attackingEnemies.append(enemy)
	if attackBouncesRemaining > 0 && attackRangeRemaining >= 0:
		var enemiesInRange = NodeExtensions.GetNode2DsWithinRange(enemy, attackRangeRemaining, Enemy)
		if enemiesInRange != null:
			for enemy2 in enemiesInRange:
				if attackBouncesRemaining > 0 && !attackingEnemies.has(enemy2):
					attackBouncesRemaining -= 1
					Attack (enemy2, enemy.rigidBody.global_position)

func TakeDamage (amount : float):
	if invulnerable:
		return
	var blockDamageEveryDamageIntervalGodEffect = NodeExtensions.GetNode(BlockDamageEveryDamageIntervalGodEffect, false, itemsParent)
	if blockDamageEveryDamageIntervalGodEffect != null:
		blockDamageEveryDamageIntervalGodEffect.damageTillBlock -= 1
		if blockDamageEveryDamageIntervalGodEffect.damageTillBlock == 0:
			blockDamageEveryDamageIntervalGodEffect.damageTillBlock += blockDamageEveryDamageIntervalGodEffect.interval
			return
	var previousHp = int(hp)
	super.TakeDamage (amount)
	var _hp = int(hp)
	if _hp > previousHp:
		for i in range(previousHp, _hp):
			var child = HP_IMAGE_SCENE.instantiate()
			hpImagesParent.add_child(child)
	else:
		for i in range(_hp, previousHp):
			var child = hpImagesParent.get_child(0)
			child.free()

func UpdateMaxHpImages ():
	var maxHpImageCount = maxHpImagesParent.get_child_count()
	if maxHp > maxHpImageCount:
		for i in range(maxHpImageCount, maxHp):
			var maxHpImage = MAX_HP_IMAGE_SCENE.instantiate()
			maxHpImagesParent.add_child(maxHpImage)
	else:
		for i in range(maxHp, maxHpImageCount):
			var child = maxHpImagesParent.get_child(0)
			child.free()
			child = hpImagesParent.get_child(0)
			child.free()

func Die ():
	var reviveWithLessMaxHpGodEffect = NodeExtensions.GetNode(ReviveWithLessMaxHpGodEffect, false, itemsParent)
	if reviveWithLessMaxHpGodEffect != null:
		maxHp -= 1
		if maxHp > 0:
			return
	var reviveWithLessCashGodEffect = NodeExtensions.GetNode(ReviveWithLessCashGodEffect, false, itemsParent)
	if reviveWithLessCashGodEffect != null:
		AddCash (-reviveWithLessCashGodEffect.loseCashAmountOnRevive)
		maxCash = cash - reviveWithLessCashGodEffect.capDifferenceOnRevive
		if cash > 0:
			return
	dead = true
	deathMenu.Open ()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	deathLabel.text = deathLabel.text.replace(REPLACE_WITH_SCORE_INDICATOR, str(int(score)))
	deathLabel.text = deathLabel.text.replace(REPLACE_WITH_HIGHSCORE_INDICATOR, str(int(highscore)))
	%GameManager.Save ()
	Room.instance.free()
	queue_free()

func AddCash (amount : float):
	cash = clampf(cash + amount, 0, maxCash)
	cashLabel.text = str(int(cash))

func SetHaggling (amount : float):
	haggling = amount

func SetEnemyCashMultiplier (amount : float):
	previousEnemyCashMultiplier = enemyCashMultiplier
	enemyCashMultiplier = amount

func HasGodEffectOfSameTier (effect : GodEffect):
	for effects in godEffectsDict.values():
		for effect2 in effects:
			if effect2.get_parent() == effect.get_parent():
				return true
	return false

extends Collectable
class_name Item

@export var cost : float
@export var maxCount : int = -1
@export var combinedWith : Array[Item]
@export var buyButton : Button
@export var buyButtonLabel : Label
@export var descriptionLabel : Label
@export var collider : CollisionObject2D
var inShop : bool

func _ready ():
	if !Room.instance.GetRect().has_point(buyButton.global_position):
		buyButton.get_parent().position = -buyButton.get_parent().position

func Collect ():
	if inShop:
		Player.instance.AddCash (-cost / (1.0 + Player.instance.haggling))
		var shopRoom = Room.instance as ShopRoom
		for itemContainer in shopRoom.itemsParent.get_children():
			var item = itemContainer.get_child(0)
			if item != null:
				item.buyButton.disabled = Player.instance.cash < cost
	for item in combinedWith:
		item.Collect ()
	if !(get_parent() is Item):
		reparent(Player.instance.itemsParent)
		position = Vector2.ZERO
		buyButton.get_parent().visible = false
		collider.set_collision_layer_value(4, false)
		Player.instance.items.append(self)
	visible = false

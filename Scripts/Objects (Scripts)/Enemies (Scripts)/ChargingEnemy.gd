extends Enemy
class_name ChargingEnemy

@export var speedMultiplierWhenCharging : float
@export var chargeRange : float
@export var chargeReloadTime : float
@export var damage : int
var chargeDirection : Vector2
var charging : bool
var chargeRangeRemaining : float
var chargeReloadTimeRemaining : float

func HandleMove ():
	var toPlayer = Player.instance.global_position - rigidBody.global_position
	if !charging:
		rigidBody.linear_velocity = toPlayer.normalized() * moveSpeed
		rigidBody.look_at(rigidBody.global_position + toPlayer)
		chargeReloadTimeRemaining -= deltaTime
		if chargeReloadTimeRemaining <= 0 && toPlayer.length_squared() <= chargeRange * chargeRange:
			chargeRangeRemaining = chargeRange
			moveSpeed *= speedMultiplierWhenCharging
			chargeDirection = VectorExtensions.FromFacingAngle(rigidBody.global_rotation_degrees)
			charging = true
	else:
		rigidBody.linear_velocity = chargeDirection * moveSpeed
		rigidBody.look_at(rigidBody.global_position + chargeDirection)
		chargeRangeRemaining -= moveSpeed * deltaTime
		if chargeRangeRemaining <= 0:
			chargeReloadTimeRemaining = chargeReloadTime
			moveSpeed /= speedMultiplierWhenCharging
			charging = false

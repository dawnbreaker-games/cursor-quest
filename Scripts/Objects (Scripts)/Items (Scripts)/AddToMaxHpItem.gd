extends Item
class_name AddToMaxHpItem

@export var amount : int

func Collect ():
	super.Collect ()
	Player.instance.maxHp += amount
	Player.instance.UpdateMaxHpImages ()

extends Item
class_name ShieldItem

@export var rotateSpeed : float
@export var shield : CanvasItem
var _rotation : float
var distance : float

func _ready ():
	distance = shield.position.length()
	set_physics_process(false)

func Collect ():
	var size = shield.global_scale
	super.Collect ()
	shield.global_scale = size
	collider.visible = false
	collider.process_mode = PROCESS_MODE_DISABLED
	visible = true
	shield.visible = true
	shield.get_child(0).disabled = false
	set_physics_process(true)

func _physics_process (deltaTime : float):
	if GameManager.paused:
		return
	shield.global_position = global_position + VectorExtensions.FromFacingAngle(_rotation) * distance
	_rotation += rotateSpeed * deltaTime

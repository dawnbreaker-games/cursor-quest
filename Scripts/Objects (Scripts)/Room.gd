extends Node2D
class_name Room

@export var size : Vector2i
@export var spawnBorder : int
static var instance : Room
var location : Vector2i
var cleared : bool
var difficulty : float

func GetRect ():
	return Rect2i(-size / 2, size)

func GetWrappedPosition (v : Vector2):
	var output = Vector2(v)
	if output.x < -size.x / 2:
		output.x += size.x
	elif output.x > size.x / 2:
		output.x -= size.x
	if output.y < -size.y / 2:
		output.y += size.y
	elif output.y > size.y / 2:
		output.y -= size.y
	return output

func Clear ():
	cleared = true
	Game.instance.roomsTillShopRoom -= 1
	Game.instance.roomsTillShrineRoom -= 1
	Game.instance.totalShopCost += Game.instance.addToTotalShopCost
	Game.instance.multiplyShopCosts += Game.instance.addToMultiplyShopCosts
	Game.instance.multiplyDifficultyTillHeart += Game.instance.addToMultiplyDifficultyTillHeart
	GodEffectMenu.godsForTiersThatWentUp.clear()
	GodEffectMenu.godsForTiersThatWentDown.clear()
	for item in Player.instance.items:
		var useableItem = item as UseableItem
		if useableItem != null && useableItem.cooldownType == UseableItem.CooldownType.RoomsCleared:
			useableItem.SetCooldown (useableItem.cooldownRemaining - 1)
	for godEffects in Player.instance.godEffectsDict.values():
		for godEffect in godEffects:
			var addToHpAfterRoomRangeGodEffect = godEffect as AddToHpAfterRoomRangeGodEffect
			if addToHpAfterRoomRangeGodEffect != null:
				addToHpAfterRoomRangeGodEffect.roomsRemaining -= 1
				if addToHpAfterRoomRangeGodEffect.roomsRemaining == 0:
					Player.instance.TakeDamage (addToHpAfterRoomRangeGodEffect.amount)
					addToHpAfterRoomRangeGodEffect.roomsRemaining = randi_range(addToHpAfterRoomRangeGodEffect.roomRange.min, addToHpAfterRoomRangeGodEffect.roomRange.max)
			else:
				var addToCashEachRoomGodEffect = godEffect as AddToCashEachRoomGodEffect
				if addToCashEachRoomGodEffect != null:
					Player.instance.AddCash (addToCashEachRoomGodEffect.amount)
				else:
					var multiplyCashEachRoomGodEffect = godEffect as MultiplyCashEachRoomGodEffect
					if multiplyCashEachRoomGodEffect != null:
						Player.instance.AddCash (Player.instance.cash * multiplyCashEachRoomGodEffect.amount - Player.instance.cash)
					else:
						var addToDamageMultiplierEachRoomGodEffect = godEffect as AddToDamageMultiplierEachRoomGodEffect
						if addToDamageMultiplierEachRoomGodEffect != null:
							Player.instance.multiplyDamage += addToDamageMultiplierEachRoomGodEffect.amount
	for god in Player.instance.prayingTo:
		god.AddPoints (Player.instance.distributePointsToGodsImPrayingTo / Player.instance.prayingTo.size())

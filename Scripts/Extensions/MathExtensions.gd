class_name MathExtensions

static func SnapToInterval (f : float, interval : float):
	if interval == 0:
		return f
	else:
		return roundf(f / interval) * interval
